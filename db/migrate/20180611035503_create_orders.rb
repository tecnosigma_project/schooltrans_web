class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.float :price
      t.string :status
      t.integer :students_transported
      t.string :seller
      t.string :school
      t.references :plan, index: true

      t.timestamps null: false
    end
  end
end
