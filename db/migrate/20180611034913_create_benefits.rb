class CreateBenefits < ActiveRecord::Migration
  def change
    create_table :benefits do |t|
      t.string :description
      t.references :plan, index: true
    end
  end
end
