class AddBenefitsAndPlans < ActiveRecord::Migration
  def change
    create_table :benefits_plans do |t|
      t.references :benefit, :plan
    end
  end
end
