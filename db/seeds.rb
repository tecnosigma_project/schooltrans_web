Plan.create(name: 'Basic', price: 10.00)
Plan.create(name: 'Max', price: 12.00)
Plan.create(name: 'Premium', price: 14.00)

Benefit.create(description: 'Cadastro de alunos (somente com dados utilizáveis pelo sistema)',
               plans: [Plan.find(1), Plan.find(2), Plan.find(3)])
Benefit.create(description: 'Cadastro de transportes escolares',
               plans: [Plan.find(1), Plan.find(2), Plan.find(3)])
Benefit.create(description: 'Cadastro de motoristas',
               plans: [Plan.find(1), Plan.find(2), Plan.find(3)])
Benefit.create(description: 'Acesso exclusivo para motoristas por meio de CNH',
               plans: [Plan.find(1), Plan.find(2), Plan.find(3)])
Benefit.create(description: 'Acesso exclusivo para instituições de ensino por meio de CNPJ',
               plans: [Plan.find(1), Plan.find(2), Plan.find(3)])
Benefit.create(description: 'Envio de senha) em casos de esquecimento',
               plans: [Plan.find(1), Plan.find(2), Plan.find(3)])
Benefit.create(description: 'Importação de arquivo de alunos',
               plans: [Plan.find(3)])
Benefit.create(description: 'Verificação de transportes escolares com assistentes',
               plans: [Plan.find(2), Plan.find(3)])
Benefit.create(description: 'Verificação de transportes escolares com vagas para alunos com necessidades especiais',
               plans: [Plan.find(2), Plan.find(3)])
Benefit.create(description: 'Verificação de transportes escolares com cadeirinhas para alunos menores',
               plans: [Plan.find(2), Plan.find(3)])
Benefit.create(description: 'Controle de vagas nos transportes escolares',
               plans: [Plan.find(1), Plan.find(2), Plan.find(3)])
Benefit.create(description: 'Aviso de motoristas com carteira expirando',
               plans: [Plan.find(2), Plan.find(3)])
Benefit.create(description: 'Alerta de motoristas com carteira expiradas',
               plans: [Plan.find(2), Plan.find(3)])
Benefit.create(description: 'Acompanhamento do translado do aluno) através do Google Maps ®',
               plans: [Plan.find(1), Plan.find(2), Plan.find(3)])
Benefit.create(description: "Envio de notificações de Check-IN e Check-OUT do aluno (saídas e chegadas em casa e na instituição de ensino) por SMS",
               plans: [Plan.find(3)])
Benefit.create(description: "Envio de notificações de Check-IN e Check-OUT do aluno (saídas e chegadas em casa e na instituição de ensino) por email",
               plans: [Plan.find(1), Plan.find(2), Plan.find(3)])
Benefit.create(description: 'Envio de notificações quando o aluno estiver ausente, com justificativa',
               plans: [Plan.find(2), Plan.find(3)])
