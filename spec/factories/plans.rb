FactoryGirl.define do
  factory :plan do
    name { %w[Basic, Max, Premium].sample }
    price 10.00
  end
end
