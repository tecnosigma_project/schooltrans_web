FactoryGirl.define do
  factory :order do
    price 12.00
    status 'Pendente'
    students_transported { Faker::Number.number(3) }
    seller { Faker::Base.regexify('[1-9]{3}.[1-9]{3}.[1-9]{3}-[1-9]{2}') }
    school { Faker::Base.regexify('[1-9]{2}.[1-9]{3}.[1-9]{3}/0001-[1-9]{2}') }
  end
end
