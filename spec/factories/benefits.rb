FactoryGirl.define do
  factory :benefit do
    description { Faker::Lorem.sentence }
  end
end
