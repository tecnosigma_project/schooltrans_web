require 'rails_helper'

RSpec.describe Regex do
  describe '.cpf' do
    it 'should check valid CPF' do
      cpf = '123.456.789-00'

      expect(described_class.cpf).to match(cpf)
    end

    it 'should check invalid CPF' do
      cpf = '123.456.789'

      expect(described_class.cpf).not_to match(cpf)
    end
  end

  describe '.cnpj' do
    it 'should check valid CNPJ' do
      cnpj = '12.345.677/0001-33'

      expect(described_class.cnpj).to match(cnpj)
    end

    it 'should check invalid CNPJ' do
      cnpj = '12.345.677/00000-11'

      expect(described_class.cnpj).not_to match(cnpj)
    end
  end

  describe '.cnh' do
    it 'should check valid CNH' do
      cnh = '11.222.333.444'

      expect(described_class.cnh).to match(cnh)
    end

    it 'should check invalid CNH' do
      cnh = '123456'

      expect(described_class.cnh).not_to match(cnh)
    end
  end

  describe '.renavam' do
    it 'should check valid RENAVAM' do
      renavam = '11.222.333.444'

      expect(described_class.renavam).to match(renavam)
    end

    it 'should check invalid RENAVAM' do
      renavam = '123456'

      expect(described_class.renavam).not_to match(renavam)
    end
  end

  describe '.license_plate' do
    it 'should check valid license plate' do
      license_plate = 'ABC-9999'

      expect(described_class.license_plate).to match(license_plate)
    end

    it 'should check invalid license plate' do
      license_plate = '123-0000'

      expect(described_class.license_plate).not_to match(license_plate)
    end
  end

  describe '.year' do
    it 'should check valid year' do
      year = '1990'

      expect(described_class.year).to match(year)
    end

    it 'should check invalid year' do
      year = 'abc'

      expect(described_class.year).not_to match(year)
    end
  end

  describe '.email' do
    it 'should check valid email' do
      email = 'abc@teste.com.br'

      expect(described_class.email).to match(email)
    end

    it 'should check invalid email' do
      email = 'abc@@'

      expect(described_class.email).not_to match(email)
    end
  end

  describe '.postal_code' do
    it 'should check valid postal code' do
      postal_code = '05612-000'

      expect(described_class.postal_code).to match(postal_code)
    end

    it 'should check invalid postal code' do
      postal_code = '03331abc'

      expect(described_class.postal_code).not_to match(postal_code)
    end
  end

  describe '.telephone' do
    it 'should check valid telephone' do
      telephone = '(11) 1234 8888'

      expect(described_class.telephone).to match(telephone)
    end

    it 'should check invalid telephone' do
      telephone = '1234 8888'

      expect(described_class.telephone).not_to match(telephone)
    end
  end

  describe '.cellphone' do
    it 'should check valid cellphone' do
      cellphone = '(11) 9 1234 8888'

      expect(described_class.cellphone).to match(cellphone)
    end

    it 'should check invalid cellphone' do
      cellphone = '1234 8888'

      expect(described_class.cellphone).not_to match(cellphone)
    end
  end

  describe '.password' do
    it 'should check valid password' do
      password = 'abcABCabc999'

      expect(described_class.password).to match(password)
    end

    it 'should check invalid password' do
      password = 'abc123s'

      expect(described_class.password).not_to match(password)
    end
  end

  describe '.user' do
    it 'should check valid user' do
      user = 'abcABCabc'

      expect(described_class.user).to match(user)
    end

    it 'should check invalid user' do
      user = 'abc'

      expect(described_class.user).not_to match(user)
    end
  end
end
