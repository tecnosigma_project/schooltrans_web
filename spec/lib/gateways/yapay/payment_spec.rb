require 'rails_helper'

RSpec.describe Payment do
  describe '.send_payload'do
    it 'should return successful POST when payment payload is sent' do
      file = File.open('spec/fixtures/payment_successfully.json')
      data = file.read
      file.close

      FactoryGirl.create(:plan, name: 'Max', price: 12.0)

      school_params = {"plan"=>"Max",
                       "seller"=>{"id"=>"111.111.111-11"},
                       "school"=>
                        {"name"=>"Escola Piu-Piu",
                         "responsible_cpf"=>"123.456.789-00",
                         "cnpj"=>"98.342.398/9284-39",
                         "kind"=>"Particular",
                         "students_transported"=>"24",
                         "address"=>
                          {"postal_code"=>"04842-150",
                           "address"=>"Avenida João Paulo Barreto",
                           "number"=>"100",
                           "complement"=>"fundos",
                           "district"=>"Jardim Castro Alves",
                           "city"=>"São Paulo",
                           "state"=>"São Paulo",
                           "latitude"=>"",
                           "longitude"=>""},
                         "contact"=>{"email"=>"contato@piupiu.com.br", "telephone"=>"(11) 4561 4130"},
                         "access"=>{"user"=>"piupiu0001", "password"=>"0x0ssi12"},
                         "status"=>"pendente",
                         "ip"=>"192.168.0.13"}}

      response = double
      allow(response).to receive(:body) { data }
      allow(RestClient).to receive(:post) { response }

      expected_result = Gateways::Yapay::Payment.send_payload(school_params).body

      result = data

      expect(expected_result).to eq(result)
    end

    it 'should return unsuccessful POST when payment payload is sent' do
      file = File.open('spec/fixtures/payment_unsuccessfully.json')
      data = file.read
      file.close

      FactoryGirl.create(:plan, name: 'Max', price: 12.0)

      school_params = {"plan"=>"Max",
                       "seller"=>{"id"=>"111.111.111-11"},
                       "school"=>
                        {"name"=>"Escola Piu-Piu",
                         "responsible_cpf"=>"123.456.789-00",
                         "cnpj"=>"98.342.398/9284-39",
                         "kind"=>"Particular",
                         "students_transported"=>"24",
                         "address"=>
                          {"postal_code"=>"04842-150",
                           "address"=>"Avenida João Paulo Barreto",
                           "number"=>"100",
                           "complement"=>"fundos",
                           "district"=>"Jardim Castro Alves",
                           "city"=>"São Paulo",
                           "state"=>"São Paulo",
                           "latitude"=>"",
                           "longitude"=>""},
                         "contact"=>{"email"=>"contato@piupiu.com.br", "telephone"=>"(11) 4561 4130"},
                         "access"=>{"user"=>"piupiu0001", "password"=>"0x0ssi12"},
                         "status"=>"pendente",
                         "ip"=>"192.168.0.13"}}

      response = double
      allow(response).to receive(:body) { data }
      allow(RestClient).to receive(:post) { response }

      expected_result = Gateways::Yapay::Payment.send_payload(school_params).body

      result = data

      expect(expected_result).to eq(result)
    end
  end
end
