require 'rails_helper'

RSpec.describe Builders::Payloads::School do
  describe '.data' do
    it 'should return valid payload with school data' do
      plan = FactoryGirl.create(:plan, name: 'Max', price: 12.0)

      school_params = {"students_transported"=>"24",
                       "seller_id"=>"111.111.111-11",
                       "name"=>"Escola Piu-Piu",
                       "responsible_cpf"=>"123.456.789-00",
                       "responsible_name"=>"José da Silva",
                       "cnpj"=>"98.342.398/9284-39",
                       "kind"=>"Particular",
                       "postal_code"=>"04842-150",
                       "address"=>"Avenida João Paulo Barreto",
                       "number"=>"100",
                       "complement"=>"fundos",
                       "district"=>"Jardim Castro Alves",
                       "city"=>"São Paulo",
                       "state"=>"São Paulo",
                       "email"=>"contato@tecnosigma.com.br",
                       "telephone"=>"(11) 4561 4130",
                       "user"=>"piupiu0001",
                       "password"=>"0x0ssi12",
                       "latitude"=>"",
                       "longitude"=>"",
                       "client_ip"=>"192.168.43.120"}

      expected_result = Builders::Payloads::School.data(plan.name, school_params)

      result = {"plan"=>"Max",
                "seller"=>{"id"=>"111.111.111-11"},
                "school"=>
                 {"name"=>"Escola Piu-Piu",
                  "responsible_cpf"=>"123.456.789-00",
                  "responsible_name"=>"José da Silva",
                  "cnpj"=>"98.342.398/9284-39",
                  "kind"=>"Particular",
                  "students_transported"=>"24",
                  "address"=>
                   {"postal_code"=>"04842-150",
                    "address"=>"Avenida João Paulo Barreto",
                    "number"=>"100",
                    "complement"=>"fundos",
                    "district"=>"Jardim Castro Alves",
                    "city"=>"São Paulo",
                    "state"=>"São Paulo",
                    "latitude"=>"",
                    "longitude"=>""},
                  "contact"=>{"email"=>"contato@tecnosigma.com.br", "telephone"=>"(11) 4561 4130"},
                  "access"=>{"user"=>"piupiu0001", "password"=>"0x0ssi12"},
                  "status"=>"pendente",
                  "ip"=>"192.168.43.120"}}

      expect(expected_result).to eq(result)
    end
  end

  describe '.create_user' do
    it 'should return valid payload to create user' do
      school_params = {"plan"=>"Max",
                       "seller"=>{"id"=>"111.111.111-11"},
                       "school"=>
                        {"name"=>"Escola Piu-Piu",
                         "responsible_cpf"=>"123.456.789-00",
                         "responsible_name"=>"José da Silva",
                         "cnpj"=>"98.342.398/9284-39",
                         "kind"=>"Particular",
                         "students_transported"=>"24",
                         "address"=>
                          {"postal_code"=>"04842-150",
                           "address"=>"Avenida João Paulo Barreto",
                           "number"=>"100",
                           "complement"=>"fundos",
                           "district"=>"Jardim Castro Alves",
                           "city"=>"São Paulo",
                           "state"=>"São Paulo",
                           "latitude"=>"",
                           "longitude"=>""},
                         "contact"=>{"email"=>"contato@tecnosigma.com.br", "telephone"=>"(11) 4561 4130"},
                         "access"=>{"user"=>"piupiu0001", "password"=>"0x0ssi12"},
                         "status"=>"pendente",
                         "ip"=>"192.168.43.120"}}

      expected_result = Builders::Payloads::School.create_user(school_params)

      result = { account_type: "2",
                 trade_name: "Escola Piu-Piu",
                 company_name: "Escola Piu-Piu",
                 cnpj: "98.342.398/9284-39",
                 email: "contato@tecnosigma.com.br",
                 name: "José da Silva",
                 cpf: "12345678900",
                 contacts: [{type_contact: "W", number_contact: "1145614130"}],
                 type_address: "B",
                 street: "Avenida João Paulo Barreto",
                 number: "100",
                 neighborhood: "Jardim Castro Alves",
                 postal_code: "04842-150",
                 city: "São Paulo",
                 state: "SP" }

      expect(expected_result).to eq(result)
    end
  end

  describe '.get_user_data' do
    it 'should return valid payload containing school specific data' do
      cpf = '123.456.789-00'
      email = 'teste@teste.com.br'

      expected_result = Builders::Payloads::School.get_user_data(cpf: cpf, email: email)

      result = { cpf: "123.456.789-00",
                 email: "teste@teste.com.br" }

      expect(expected_result).to eq(result)
    end
  end
end
