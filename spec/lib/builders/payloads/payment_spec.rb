require 'rails_helper'

RSpec.describe Builders::Payloads::Payment do
  describe '.data' do
    it 'should return valid payment payload' do
      FactoryGirl.create(:plan, name: 'Max', price: 12.0)

      school_params = {"plan"=>"Max",
                       "seller"=>{"id"=>"111.111.111-11"},
                       "school"=>
                        {"name"=>"Escola Piu-Piu",
                         "responsible_cpf"=>"123.456.789-00",
                         "cnpj"=>"98.342.398/9284-39",
                         "kind"=>"Particular",
                         "students_transported"=>"24",
                         "address"=>
                          {"postal_code"=>"04842-150",
                           "address"=>"Avenida João Paulo Barreto",
                           "number"=>"100",
                           "complement"=>"fundos",
                           "district"=>"Jardim Castro Alves",
                           "city"=>"São Paulo",
                           "state"=>"São Paulo",
                           "latitude"=>"",
                           "longitude"=>""},
                         "contact"=>{"email"=>"contato@piupiu.com.br", "telephone"=>"(11) 4561 4130"},
                         "access"=>{"user"=>"piupiu0001", "password"=>"0x0ssi12"},
                         "status"=>"pendente",
                         "ip"=>"192.168.0.13"}}

      expected_result = Builders::Payloads::Payment.data(school_params)

      result = {:token_account=>"30b977b66623691",
                :customer=>
                 {:contacts=>[{:type_contact=>"W", :number_contact=>"1145614130"}],
                  :addresses=>
                   [{:type_address=>"B",
                     :postal_code=>"04842-150",
                     :street=>"Avenida João Paulo Barreto",
                     :number=>"100",
                     :completion=>"fundos",
                     :neighborhood=>"Jardim Castro Alves",
                     :city=>"São Paulo",
                     :state=>"SP"}],
                  :name=>"Escola Piu-Piu",
                  :cpf=>'12345678900',
                  :cnpj=>"98.342.398/9284-39",
                  :email=>"contato@piupiu.com.br"},
                :transaction_product=>
                 [{:description=>"Assinatura do Plano Max",
                   :quantity=>"1",
                   :price_unit=>"288.00",
                   :code=>"Max",
                   :extra=>"Plano disponível para 24 alunos transportados"}],
                :transaction=>
                 {:available_payment_methods=>"6",
                  :customer_ip=>"192.168.0.13",
                  :url_notification=>"http://localhost:4000/payment_notifications/"},
                :payment=>{:payment_method_id=>"6"}}

      expect(expected_result).to eq(result)
    end
  end
end
