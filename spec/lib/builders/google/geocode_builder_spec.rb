require 'rails_helper'

RSpec.describe Builders::Google::GeocodeBuilder do
  describe '.url' do
    it 'returns Google URL to search coordinates' do
      result = 'http://maps.google.com/maps/api/geocode/json?address=Avenida+Paulista+1000+Jardim+Paulista+São+Paulo+São+Paulo&sensor=false'

      expected_result = described_class.url(address: 'Avenida Paulista',
                                            number: '1000',
                                            district: 'Jardim Paulista',
                                            city: 'São Paulo',
                                            state: 'São Paulo')

      expect(expected_result).to eq(result)
    end
  end
end
