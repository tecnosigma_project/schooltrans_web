require 'rails_helper'

RSpec.describe States do
  describe '.all_names' do
    it 'should return all state names' do
      result = ['Acre',
                'Alagoas',
                'Amapá',
                'Amazonas',
                'Bahia',
                'Ceará',
                'Distrito Federal',
                'Espírito Santo',
                'Goiás',
                'Maranhão',
                'Mato Grosso',
                'Mato Grosso do Sul',
                'Minas Gerais',
                'Paraná',
                'Paraíba',
                'Pará',
                'Pernambuco',
                'Piauí',
                'Rio Grance do Sul',
                'Rio Grande do Norte',
                'Rio de Janeiro',
                'Rondônia',
                'Roraima',
                'Santa Catarina',
                'Sergipe',
                'São Paulo',
                'Tocantins']

      expect(States.all_names).to eq(result)
    end
  end

  describe '.all_federative_units' do
    it 'should return all federative unit list' do
      result = %w[AC
                  AL
                  AP
                  AM
                  BA
                  CE
                  DF
                  ES
                  GO
                  MA
                  MT
                  MS
                  MG
                  PR
                  PB
                  PA
                  PE
                  PI
                  RS
                  RN
                  RJ
                  RO
                  RR
                  SC
                  SE
                  SP
                  TO]

      expect(States.all_federative_units).to eq(result)
    end
  end

  describe '.state_name' do
    it 'should return state name when pass federative unit' do
      federative_unit = 'AM'
      result = 'Amazonas'

      expect(States.state_name(federative_unit)).to eq(result)
    end
  end

  describe '.federative_unit' do
    it 'should return federative unit when pass state name' do
      state_name = 'Amazonas'
      result = 'AM'

      expect(States.federative_unit(state_name)).to eq(result)
    end
  end
end
