require 'rails_helper'

RSpec.describe CheckoutController, type: :controller do
  describe 'GET actions' do
    describe '#payment' do
      it 'should return http status 200' do
        mock_to_existent_user
        mock_to_payment_sucessfully

        get :payment

        expect(response).to have_http_status(200)
      end
    end
    describe '#seller_name' do
      it 'should return http status 200 when seller name is found' do
        seller_cpf = '111.111.111-11'

        dbl_response = double
        allow(dbl_response).to receive(:body) { "{\"seller_name\":\"José da Silva\"}" }
        allow(dbl_response).to receive(:code) { 200 }

        expect(RestClient).to receive(:post) { dbl_response }

        get :seller_name, seller_id: seller_cpf

        expect(response).to have_http_status(200)
      end

      it 'should return http status 200 when seller name isn\'t found' do
        seller_cpf = 'anything'

        dbl_response = double
        allow(dbl_response).to receive(:body) { '' }
        allow(dbl_response).to receive(:code) { 200 }

        expect(RestClient).to receive(:post) { dbl_response }


        get :seller_name, seller_id: seller_cpf

        expect(response).to have_http_status(200)
      end

      it 'should return http status 200 when occurs some errors' do
        expect(RestClient).to receive(:post) { StandardError }

        seller_cpf = '505.693.950-82'

        get :seller_name, seller_id: seller_cpf

        expect(response).to have_http_status(200)
      end
    end

    describe '#school_data' do
      it 'returns status http 302 when pass plan name' do
        session[:plan_name] = 'Basic'

        params = {"plan"=>session[:plan_name],
          "seller"=>{"id"=>"505.693.950-82"},
          "school"=>
          {"name"=>"Escola do Piu-Piu",
          "responsible_name"=>"José da Silva",
          "responsible_cpf"=>"123.456.789-00",
          "cnpj"=>"43.342.983/5843-98",
          "kind"=>"Particular",
          "address"=>
          {"postal_code"=>"31110-050",
          "address"=>"Rua Jacuí",
          "number"=>"300",
          "complement"=>"sobreloja",
          "district"=>"Floresta",
          "city"=>"Belo Horizonte",
          "state"=>"Minas Gerais",
          "latitude"=>"-19.9079808",
          "longitude"=>"-43.9323068"},
          "contact"=>{"email"=>"tecnooxossi@gmail.com", "telephone"=>"(41) 2343 2434"},
          "access"=>{"user"=>"piupiu", "password"=>"0x0ssi12"}}}

        get :school_data, params

        expect(response).to have_http_status(302)
      end

      it 'returns status http 302 when don\'t pass plan name' do
        params = {"plan"=>session[:plan_name],
          "seller"=>{"id"=>"505.693.950-82"},
          "school"=>
          {"name"=>"Escola do Piu-Piu",
          "responsible_name"=>"José da Silva",
          "responsible_cpf"=>"123.456.789-00",
          "cnpj"=>"43.342.983/5843-98",
          "kind"=>"Particular",
          "address"=>
          {"postal_code"=>"31110-050",
          "address"=>"Rua Jacuí",
          "number"=>"300",
          "complement"=>"sobreloja",
          "district"=>"Floresta",
          "city"=>"Belo Horizonte",
          "state"=>"Minas Gerais",
          "latitude"=>"-19.9079808",
          "longitude"=>"-43.9323068"},
          "contact"=>{"email"=>"tecnooxossi@gmail.com", "telephone"=>"(41) 2343 2434"},
          "access"=>{"user"=>"piupiu", "password"=>"0x0ssi12"}}}

        get :school_data, params

        expect(response).to have_http_status(302)
      end
    end

    describe '#school_coordinates' do
      it 'returns status http 200 when data are valid' do
        result = {"latitude"=>-23.5702861, "longitude"=>-46.645112}

        allow(Coordinates).to receive(:parsed_coordinates) { result }

        get :school_coordinates, { address: 'Avenida Paulista',
                                   number: '100',
                                   district: 'Planalto Paulista ',
                                   city: 'São Paulo',
                                   state: 'São Paulo',
                                   postal_code: '04080-923' }

        expect(response).to have_http_status(200)
      end

      it 'returns status http 200 when data are invalid' do
        result = {"latitude"=>"", "longitude"=>""}

        allow(Coordinates).to receive(:parsed_coordinates) { result }

        get :school_coordinates, { address: nil,
                                   number: nil,
                                   district: nil,
                                   city: nil,
                                   state: nil,
                                   postal_code: nil }

        expect(response).to have_http_status(200)
      end
    end

    describe '#identification' do
      it 'should return http status 200 when pass valid plan name' do
        plan = FactoryGirl.create(:plan, name: 'Max')

        get :identification, plan: plan.name

        expect(response).to have_http_status(200)
      end

      it 'should return http status 302 when pass invalid plan name' do
        invalid_plan = 'anything'

        get :identification, plan: invalid_plan

        expect(response).to have_http_status(302)
      end

      it 'should return http status 302 when pass null params' do
        get :identification, plan: nil

        expect(response).to have_http_status(302)
      end
    end

    describe '#school_user_availability' do
      it 'should return http status 200 when pass existent user' do
        user = 'bertrand'

        dbl_response = double
        allow(dbl_response).to receive(:body) { '' }
        allow(dbl_response).to receive(:code) { 200 }

        expect(RestClient).to receive(:post) { dbl_response }

        get :school_user_availability, user: user

        expect(response).to have_http_status(200)
      end

      it 'should return http status 200 when pass non existent user' do
        user = 'anything'

        dbl_response = double
        allow(dbl_response).to receive(:body) { '' }
        allow(dbl_response).to receive(:code) { 200 }

        expect(RestClient).to receive(:post) { dbl_response }

        get :school_user_availability, user: user

        expect(response).to have_http_status(200)
      end
    end

    describe '#school_address' do
      it 'should return http status 200 when pass valid postal code' do
        postal_code = '69088-240'

        params_result = { neighborhood: "Jorge Teixeira",
                          zipcode: "69088240",
                          city: "Manaus",
                          complement: "",
                          address: "Avenida Itaúba",
                          state: "AM" }

        expect(Correios::CEP::AddressFinder)
          .to receive(:get)
          .with(postal_code)
          .and_return(params_result)

        result = "{\"neighborhood\":\"Jorge Teixeira\",\"zipcode\":\"69088240\",\"city\":\"Manaus\",\"complement\":\"\",\"address\":\"Avenida Itaúba\",\"state\":\"Amazonas\"}"

        get :school_address, postal_code: postal_code

        expect(response).to have_http_status(200)
      end

      it 'should not return status http 404 when pass invalid postal code' do
        allow(Correios::CEP::AddressFinder).to receive(:get) { StandardError }

        postal_code = '69088-240'

        get :school_address, postal_code: postal_code

        expect(response).to have_http_status(404)
      end
    end

    describe '#resume' do
      it 'should return http status 200 when pass valid plan name' do
        plan = FactoryGirl.create(:plan, name: 'Max')

        session[:plan_name] = plan.name

        get :resume

        expect(response).to have_http_status(200)
      end
    end
  end

  describe '#identification' do
    it 'should return session containing plan name when pass valid plan name' do
      valid_plan = 'max'

      get :identification, plan: valid_plan

      expect(session[:plan_name]).to be_present
    end

    it 'should return the assign of step name to hire plan' do
      valid_plan = 'max'

      result = 'identification'

      get :identification, plan: valid_plan

      expect(assigns[:step]).to eq(result)
    end

    it 'should redirect to root path when pass invalid plan name' do
      invalid_plan = 'anything'

      get :identification, plan: invalid_plan

      expect(response).to redirect_to(root_path)
    end

    it 'should redirect to root path when pass null params' do
      get :identification, plan: nil

      expect(response).to redirect_to(root_path)
    end
  end

  describe '#school_address' do
    it 'should return school address in JSON format when pass valid postal code format and found address' do
      postal_code = '69088-240'

      params_result = { neighborhood: "Jorge Teixeira",
                        zipcode: "69088240",
                        city: "Manaus",
                        complement: "",
                        address: "Avenida Itaúba",
                        state: "AM" }

      expect(Correios::CEP::AddressFinder)
        .to receive(:get)
        .with(postal_code)
        .and_return(params_result)

      result = "{\"neighborhood\":\"Jorge Teixeira\",\"zipcode\":\"69088240\",\"city\":\"Manaus\",\"complement\":\"\",\"address\":\"Avenida Itaúba\",\"state\":\"Amazonas\"}"

      get :school_address, postal_code: postal_code

      expect(response.body).to eq(result)
    end

    it 'should return empty JSON when pass valid postal code format and not found address' do
      postal_code = '99999-000'

      params_result = Hash.new

      expect(Correios::CEP::AddressFinder)
        .to receive(:get)
        .with(postal_code)
        .and_return(params_result)

      result = params_result.to_s

      get :school_address, postal_code: postal_code


      expect(response.body).to eq(result)
    end

    it 'should return empty JSON when pass invalid postal code' do
      postal_code = 'anything'

      params_result = Hash.new

      expect(Correios::CEP::AddressFinder)
        .to receive(:get)
        .with(postal_code)
        .and_return(params_result)

      result = params_result.to_s

      get :school_address, postal_code: postal_code

      expect(response.body).to eq(result)
    end

    it 'should return empty JSON when occurs some errors' do
      postal_code = '69088-240'

      expect(Correios::CEP::AddressFinder)
        .to receive(:get)
        .with(postal_code)
        .and_return(StandardError)

      result = Hash.new.to_s

      get :school_address, postal_code: postal_code

      expect(response.body).to eq(result)
    end
  end

  describe '#resume' do
    it 'should return the assign of benefit list when pass valid plan' do
      benefit = FactoryGirl.create(:benefit)
      plan = FactoryGirl.create(:plan, name: 'Max', benefits: [benefit])

      session[:plan_name] = plan.name

      get :resume

      expect(assigns[:benefit_list]).to be_present
    end

    it 'should return the assign of plan name when pass valid plan' do
      plan = FactoryGirl.create(:plan, name: 'Max')

      session[:plan_name] = plan.name

      get :resume

      expect(assigns[:plan_name]).to be_present
    end

    it 'should return the assign of step name to hire plan' do
      plan = FactoryGirl.create(:plan, name: 'Max')

      session[:plan_name] = plan.name

      result = 'resume'

      get :resume

      expect(assigns[:step]).to eq(result)
    end

    it 'should return the assign of plan name when pass valid plan' do
      benefit = FactoryGirl.create(:benefit)
      plan = FactoryGirl.create(:plan, name: 'Max', benefits: [benefit])

      session[:plan_name] = plan.name
      session[:school_data] = 'anything'

      get :resume

      expect(assigns[:school_data]).to be_present
    end
  end

  describe '#school_user_availability' do
    it 'should inform that user is available when user isn\'t found' do
      user = 'anything'

      result = "{\"available\":true}"

      dbl_response = double
      allow(dbl_response).to receive(:body) { result }
      allow(dbl_response).to receive(:code) { 200 }

      expect(RestClient).to receive(:post) { dbl_response }

      get :school_user_availability, user: user

      expect(response.body).to eq(result)
    end

    it 'should inform that user is unavailable when user is found' do
      user = 'bertrand'

      result = "{\"available\":false}"

      dbl_response = double
      allow(dbl_response).to receive(:body) { result }
      allow(dbl_response).to receive(:code) { 204 }

      expect(RestClient).to receive(:post) { dbl_response }

      get :school_user_availability, user: user

      expect(response.body).to eq(result)
    end

    it 'should inform that user is unavailable when occurs some errors' do
      expect(RestClient).to receive(:post) { StandardError }

      user = 'anything'

      result = "{\"available\":false}"



      get :school_user_availability, user: user

      expect(response.body).to eq(result)
    end
  end

  describe '#school_coordinates' do
    it 'returns school coordinates when data are valid' do
      result = {"latitude"=>-23.5702861, "longitude"=>-46.645112}

      allow(Coordinates).to receive(:parsed_coordinates) { result }

      get :school_coordinates, { address: 'Avenida Paulista',
                                 number: '100',
                                 district: 'Planalto Paulista ',
                                 city: 'São Paulo',
                                 state: 'São Paulo',
                                 postal_code: '04080-923' }

      expect(JSON.parse(response.body)).to eq(result)
    end

    it 'returns empty coordinates when data are invalid' do
      result = {"latitude"=>"", "longitude"=>""}

      allow(Coordinates).to receive(:parsed_coordinates) { result }

      get :school_coordinates, { address: nil,
                                 number: nil,
                                 district: nil,
                                 city: nil,
                                 state: nil,
                                 postal_code: nil }

      expect(JSON.parse(response.body)).to eq(result)
    end
  end

  describe '#school_data' do
    it 'should return the assign of school data when pass valid plan' do
      session[:plan_name] = 'Basic'

      params = {"plan"=>session[:plan_name],
                "seller"=>{"id"=>"505.693.950-82"},
                "school"=>
                {"name"=>"Escola do Piu-Piu",
                "cnpj"=>"43.342.983/5843-98",
                "kind"=>"Particular",
                "address"=>
                {"postal_code"=>"31110-050",
                "address"=>"Rua Jacuí",
                "number"=>"300",
                "complement"=>"sobreloja",
                "district"=>"Floresta",
                "city"=>"Belo Horizonte",
                "state"=>"Minas Gerais",
                "latitude"=>"-19.9079808",
                "longitude"=>"-43.9323068"},
                "contact"=>{"email"=>"tecnooxossi@gmail.com", "telephone"=>"(41) 2343 2434"},
                "access"=>{"user"=>"piupiu", "password"=>"0x0ssi12"},
                "status": "pendente"}}

      get :school_data, params

      expect(session[:school_data]).to be_present
    end

    it 'should return the assign of school data when pass valid plan' do
      session[:plan_name] = 'Basic'

      params = {"plan"=>session[:plan_name],
                "seller"=>{"id"=>"505.693.950-82"},
                "school"=>
                {"name"=>"Escola do Piu-Piu",
                "cnpj"=>"43.342.983/5843-98",
                "kind"=>"Particular",
                "address"=>
                {"postal_code"=>"31110-050",
                "address"=>"Rua Jacuí",
                "number"=>"300",
                "complement"=>"sobreloja",
                "district"=>"Floresta",
                "city"=>"Belo Horizonte",
                "state"=>"Minas Gerais",
                "latitude"=>"-19.9079808",
                "longitude"=>"-43.9323068"},
                "contact"=>{"email"=>"tecnooxossi@gmail.com", "telephone"=>"(41) 2343 2434"},
                "access"=>{"user"=>"piupiu", "password"=>"0x0ssi12"},
                "status": "pendente"}}

      get :school_data, params

      expect(response).to redirect_to(checkout_resume_path)
    end

    it 'should not return the assign of school data when pass invalid plan' do
      session[:plan_name] = nil

      params = {"plan"=>session[:plan_name],
                "seller"=>{"id"=>"505.693.950-82"},
                "school"=>
                {"name"=>"Escola do Piu-Piu",
                "cnpj"=>"43.342.983/5843-98",
                "kind"=>"Particular",
                "address"=>
                {"postal_code"=>"31110-050",
                "address"=>"Rua Jacuí",
                "number"=>"300",
                "complement"=>"sobreloja",
                "district"=>"Floresta",
                "city"=>"Belo Horizonte",
                "state"=>"Minas Gerais",
                "latitude"=>"-19.9079808",
                "longitude"=>"-43.9323068"},
                "contact"=>{"email"=>"tecnooxossi@gmail.com", "telephone"=>"(41) 2343 2434"},
                "access"=>{"user"=>"piupiu", "password"=>"0x0ssi12"},
                "status": "pendente"}}

      get :school_data, params

      expect(assigns[:school_data]).to be_nil
    end
  end

  describe '#seller_name' do
    it 'should return seller name in JSON format when seller is found' do
      seller_cpf = '111.111.111-11'
      result = "{\"seller_name\":\"José da Silva\"}"

      dbl_response = double
      allow(dbl_response).to receive(:body) { result }
      allow(dbl_response).to receive(:code) { 200 }

      expect(RestClient).to receive(:post) { dbl_response }

      get :seller_name, seller_id: seller_cpf

      expect(response.body).to eq(result)
    end

    it 'should return empty JSON when seller isn\'t found' do
      seller_cpf = 'anything'
      result = "{\"seller_name\":\"\"}"

      dbl_response = double
      allow(dbl_response).to receive(:body) { result }
      allow(dbl_response).to receive(:code) { 200 }

      expect(RestClient).to receive(:post) { dbl_response }

      get :seller_name, seller_id: seller_cpf

      expect(response.body).to eq(result)
    end

    it 'should return empty JSON when occurs some errors' do
      expect(RestClient).to receive(:post) { StandardError }

      seller_cpf = '505.693.950-82'

      result = "{\"seller_name\":\"\"}"

      get :seller_name, seller_id: seller_cpf

      expect(response.body).to eq(result)
    end
  end

  describe '#payment' do
    context 'when does exist user in payment gateway' do
      it 'should return the assign of plan name when payload is sent' do
        mock_to_existent_user
        mock_to_payment_sucessfully

        result = session[:school_data]['plan']

        get :payment

        expect(assigns[:plan_name]).to eq(result)
      end

      it 'should return the assign of school email when payload is sent' do
        mock_to_existent_user
        mock_to_payment_sucessfully

        result = session[:school_data]['school']['contact']['email']

        get :payment

        expect(assigns[:email]).to eq(result)
      end

      it 'should return the assign of step name to hire plan' do
        mock_to_existent_user
        mock_to_payment_sucessfully

        result = 'payment'

        get :payment

        expect(assigns[:step]).to eq(result)
      end
    end

    context 'when doesn\'t exist user in payment gateway' do
      it 'should return the assign of plan name when payload is sent' do
        mock_to_non_existent_user
        mock_to_payment_sucessfully

        result = session[:school_data]['plan']

        get :payment

        expect(assigns[:plan_name]).to eq(result)
      end

      it 'should return the assign of school email when payload is sent' do
        mock_to_non_existent_user
        mock_to_payment_sucessfully

        result = session[:school_data]['school']['contact']['email']

        get :payment

        expect(assigns[:email]).to eq(result)
      end

      it 'should return the assign of step name to hire plan' do
        mock_to_non_existent_user

        expect(Builders::Payloads::School)
          .to receive(:create_user)
          .and_return('anything')

        file = File.open('spec/fixtures/created_school_successfully.json')
        data = file.read
        file.close

        response = double
        allow(response).to receive(:body) { data }
        allow(response).to receive(:code) { 200 }

        expect(RestClient)
          .to receive(:post)
          .and_return(response)

        mock_to_payment_sucessfully

        result = 'payment'

        get :payment

        expect(assigns[:step]).to eq(result)
      end
    end
  end

  def mock_to_payment_sucessfully
    file = File.open('spec/fixtures/payment_successfully.json')
    data = file.read
    file.close

    response = double
    allow(response).to receive(:body) { data }
    allow(response).to receive(:code) { 201 }

    session[:school_data] = {'plan'=> 'Max',
                             'school'=> {'responsible_cpf'=> '111.111.111-11',
                                         'contact'=> {'email'=>'anything@anything.com.br'}}}

    expect(Gateways::Yapay::Payment)
      .to receive(:send_payload)
      .with(session[:school_data])
      .and_return(response)
  end

  def mock_to_existent_user
    cpf = '111.111.111-11'
    email = 'anything@anything.com.br'

    expect(Builders::Payloads::School)
      .to receive(:get_user_data)
      .and_return({ cpf: cpf,  email: email })

    client_found_file = File.open('spec/fixtures/client_found.xml')
    client_found_data = client_found_file.read
    client_found_file.close

    response = double
    allow(response).to receive(:body) { client_found_data }
    allow(response).to receive(:code) { 200 }

    expect(RestClient)
      .to receive(:post)
      .and_return(response)
  end

  def mock_to_non_existent_user
    cpf = '111.111.111-11'
    email = 'anything@anything.com.br'

    expect(Builders::Payloads::School)
      .to receive(:get_user_data)
      .and_return({ cpf: cpf,  email: email })

    client_found_file = File.open('spec/fixtures/client_not_found.xml')
    client_found_data = client_found_file.read
    client_found_file.close

    response = double
    allow(response).to receive(:body) { client_found_data }
    allow(response).to receive(:code) { 200 }

    expect(RestClient)
      .to receive(:post)
      .and_return(response)
  end
end
