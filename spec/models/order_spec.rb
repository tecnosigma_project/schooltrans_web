require 'rails_helper'

RSpec.describe Order, type: :model do
  describe 'validates presences' do
    it 'should return valid order when pass nil value  in \'price\'' do
      order = FactoryGirl.build(:order, price: nil)

      expect(order).to be_invalid
    end

    it 'should return valid order when pass nil value  in \'status\'' do
      order = FactoryGirl.build(:order, status: nil)

      expect(order).to be_invalid
    end

    it 'should return valid order when pass nil value  in \'students_transported\'' do
      order = FactoryGirl.build(:order, students_transported: nil)

      expect(order).to be_invalid
    end

    it 'should return valid order when pass nil value  in \'seller\'' do
      order = FactoryGirl.build(:order, seller: nil)

      expect(order).to be_invalid
    end

    it 'should return valid order when pass nil value  in \'school\'' do
      order = FactoryGirl.build(:order, school: nil)

      expect(order).to be_invalid
    end
  end

  describe 'validates relationship with Plan entity' do
    it 'shoud to have relationship N:1' do
      order = FactoryGirl.create(:order)

      expect(order).to respond_to(:plan)
    end
  end
end
