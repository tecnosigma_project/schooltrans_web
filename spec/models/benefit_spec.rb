require 'rails_helper'

RSpec.describe Benefit, type: :model do
  describe 'validates presences' do
    it 'should return valid benefit when pass nil value  in \'benefit\'' do
      benefit = FactoryGirl.build(:benefit, description: nil)

      expect(benefit).to be_invalid
    end
  end

  describe 'validates relationship with Plan entity' do
    it 'shoud to have relationship N:N' do
      benefit = FactoryGirl.create(:benefit)

      expect(benefit).to respond_to(:plans)
    end
  end
end
