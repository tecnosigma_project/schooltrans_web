require 'rails_helper'

RSpec.describe Plan, type: :model do
  describe 'validates presences' do
    it 'should return valid plan when pass nil value  in \'name\'' do
      plan = FactoryGirl.build(:plan, name: nil)

      expect(plan).to be_invalid
    end

    it 'should return valid plan when pass nil value  in \'price\'' do
      plan = FactoryGirl.build(:plan, price: nil)

      expect(plan).to be_invalid
    end
  end

  describe 'validates relationship with Benefit entity' do
    it 'shoud to have relationship N:N' do
      plan = FactoryGirl.create(:plan)

      expect(plan).to respond_to(:benefits)
    end
  end

  describe 'validates relationship with Order entity' do
    it 'shoud to have relationship 1:N' do
      plan = FactoryGirl.create(:plan)

      expect(plan).to respond_to(:orders)
    end
  end

  describe '.total_price' do
    it 'should total price with base in student amount' do
      students_transported = 10
      plan_price = 2.0

      result = 20.0

      expect(described_class.total_price(students_transported, plan_price))
        .to eq(result)
    end
  end

  describe '.description' do
    it 'should return descritive message about plan' do
      plan = FactoryGirl.create(:plan, name: 'max')

      result = "Assinatura do Plano #{plan.name.titleize}"

      expect(described_class.description(plan.name)).to eq(result)
    end
  end

  describe '.availability_description' do
    it 'should return descritive message plan availability' do
      student_amount = 30

      result = "Plano disponível para #{student_amount} alunos transportados"

      expect(described_class.availability_description(student_amount))
        .to eq(result)
    end
  end
end
