require 'rails_helper'

RSpec.describe CheckoutHelper, type: :helper do
  include CheckoutHelper

  describe '#school_types' do
    it 'returns state type list' do
      result = %w[Estadual
                  Federal
                  Municipal
                  Particular]

      expect(school_types).to eq(result)
    end
  end

  describe '#information_rule' do
    it 'should return information rule with active label \'identification\' when current step is \'identification\'' do
      step = 'identification'

      result = "<li class='actived-step'>Identificação</li>
      <li><span class='glyphicon glyphicon-play'></li>
      <li class='deactivated-step'>Resumo</li>
      <li><span class='glyphicon glyphicon-play'></li>
      <li class='deactivated-step'>Pagamento</li>"

      expect(information_rule(step)).to eq(result)
    end

    it 'should return information rule with active label \'resume\' when current step is \'resume\'' do
      step = 'resume'

      result = "<li class='deactivated-step'>Identificação</li>
      <li><span class='glyphicon glyphicon-play'></li>
      <li class='actived-step'>Resumo</li>
      <li><span class='glyphicon glyphicon-play'></li>
      <li class='deactivated-step'>Pagamento</li>"

      expect(information_rule(step)).to eq(result)
    end

    it 'should return information rule with active label \'payment\' when current step is \'payment\'' do
      step = 'payment'

      result = "<li class='deactivated-step'>Identificação</li>
      <li><span class='glyphicon glyphicon-play'></li>
      <li class='deactivated-step'>Resumo</li>
      <li><span class='glyphicon glyphicon-play'></li>
      <li class='actived-step'>Pagamento</li>"

      expect(information_rule(step)).to eq(result)
    end
  end
end
