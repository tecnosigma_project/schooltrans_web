require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  include ApplicationHelper

  describe '#formatted_date' do
    it 'returns formatted date' do
      date = DateTime.now

      expect(formatted_date(date)).to eq(date.strftime('%d/%m/%Y'))
    end
  end

  describe '#formatted_date' do
    it 'returns formatted date' do
      time = Time.now

      expect(formatted_time(time)).to eq(time.strftime('%H:%M'))
    end
  end

  describe '#boolean_options' do
    it 'returns options that contains values boolean (Não e Sim) to tag select' do
      result = [['Não', 0], ['Sim', 1]]

      expect(boolean_options).to eq(result)
    end
  end

  describe '#bool_to_string' do
    it 'returns value \'Sim\' when boolean value is true' do
      result          = 'Sim'
      expected_result = bool_to_string(true)

      expect(expected_result).to eq(result)
    end

    it 'returns value \'Não\' when boolean value is false' do
      result          = 'Não'
      expected_result = bool_to_string(false)

      expect(expected_result).to eq(result)
    end
  end

  describe '#bool_to_integer' do
    it 'returns value \'1\' when boolean value is true' do
      result          = 1
      expected_result = bool_to_integer(true)

      expect(expected_result).to eq(result)
    end

    it 'returns value \'0\' when boolean value is false' do
      result          = 0
      expected_result = bool_to_integer(false)

      expect(expected_result).to eq(result)
    end
  end

  describe '#state_list' do
    it 'returns state list' do
      result = ['Acre',
                'Alagoas',
                'Amapá',
                'Amazonas',
                'Bahia',
                'Ceará',
                'Distrito Federal',
                'Espírito Santo',
                'Goiás',
                'Maranhão',
                'Mato Grosso',
                'Mato Grosso do Sul',
                'Minas Gerais',
                'Paraná',
                'Paraíba',
                'Pará',
                'Pernambuco',
                'Piauí',
                'Rio Grance do Sul',
                'Rio Grande do Norte',
                'Rio de Janeiro',
                'Rondônia',
                'Roraima',
                'Santa Catarina',
                'Sergipe',
                'São Paulo',
                'Tocantins']

      expect(state_list).to eq(result)
    end
  end
end
