require 'rails_helper'

RSpec.describe HomeHelper, type: :helper do
  include HomeHelper

  describe '#plans_benefit' do
    it 'returns plan benefit list' do
      plan_1 = FactoryGirl.create(:plan, name: Plan::BASIC)
      plan_2 = FactoryGirl.create(:plan, name: Plan::PREMIUM)
      plan_3 = FactoryGirl.create(:plan, name: Plan::MAX)
      benefit = FactoryGirl.create(:benefit,
                                   description: 'Teste',
                                   plans: [plan_1, plan_2, plan_3])

      expect(plans_benefit).to be_present
    end
  end

  describe '#load_image' do
    it 'returns image tag that contains image (yes)' do
      plan = FactoryGirl.create(:plan, name: Plan::BASIC)
      benefit = FactoryGirl
        .create(:benefit, description: 'Teste', plans: [plan])

      result = "<td><img src='images/yes.png' alt='Sim'></td>"

      expect(load_image(plan.name, benefit.description)).to eq(result)
    end

    it 'returns image tag that contains image (no)' do
      plan = FactoryGirl.create(:plan, name: Plan::BASIC)
      benefit = FactoryGirl
        .create(:benefit, description: 'Teste', plans: [plan])

      result = "<td><img src='images/no.png' alt='Não'></td>"

      expect(load_image(plan.name, 'Anything')).to eq(result)
    end
  end

  describe '#plans_name' do
    it 'returns header containing plans name' do
      plan_1 = FactoryGirl.create(:plan, name: 'Basic')
      plan_2 = FactoryGirl.create(:plan, name: 'Max')
      plan_3 = FactoryGirl.create(:plan, name: 'Premium')

      result = "<th>#{plan_1.name}</th><th>#{plan_2.name}</th><th>#{plan_3.name}</th>"

      expect(plans_name).to eq(result)
    end
  end

  describe '#plans_price' do
    it 'returns plans price list' do
      plan_1 = FactoryGirl.create(:plan, name: 'Basic', price: 10.0)
      plan_2 = FactoryGirl.create(:plan, name: 'Max', price: 12.0)
      plan_3 = FactoryGirl.create(:plan, name: 'Premium', price: 14.0)

      result = '<td>R$ 10,00/aluno</td><td>R$ 12,00/aluno</td><td>R$ 14,00/aluno</td>'

      expect(plans_price).to eq(result)
    end
  end

  describe '#field_with_image' do
    it 'returns field containing image' do
      image_name = 'yes'
      description = 'Sim'
      result = "<td><img src='images/yes.png' alt='Sim'></td>"

      expect(field_with_image(image_name, description)).to eq(result)
    end
  end
end
