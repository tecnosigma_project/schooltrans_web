require 'rails_helper'

RSpec.describe 'Routes', type: :routing do
  context 'HomeControler' do
    context 'GET actions' do
      it { expect(get('/')).to route_to('home#index') }
    end
  end

  context 'CheckoutController' do
    context 'GET routes' do
      it {
        expect(get('checkout/identification'))
          .to route_to('checkout#identification')
      }
      it {
        expect(get('checkout/resume'))
          .to route_to('checkout#resume')
      }
      it {
        expect(get('checkout/payment'))
          .to route_to('checkout#payment')
      }
      it {
        expect(get('checkout/school_address'))
          .to route_to('checkout#school_address')
      }
      it {
        expect(get('school_user_availability'))
          .to route_to('checkout#school_user_availability')
      }
      it {
        expect(get('school_coordinates'))
          .to route_to('checkout#school_coordinates')
      }
      it {
        expect(get('seller_name'))
          .to route_to('checkout#seller_name')
      }
    end

    context 'POST routes' do
      it {
        expect(post('checkout/school_data'))
          .to route_to('checkout#school_data')
      }
    end
  end
end
