# SchoolTrans

This project born to help school cars management.

### Prerequisites

The application is in docker container, execute the command `docker-compose up` to start the app.

To generate correctly the development database, execute the command below:

`sh generate_database.sh <DATABASE_USER> <DATABASE_PASSWORD>`
