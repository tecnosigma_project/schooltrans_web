class Plan < ActiveRecord::Base
  UNIT = '/aluno'
  BASIC = 'Basic'
  MAX = 'Max'
  PREMIUM = 'Premium'
  ALL_PLAN_TYPES = [BASIC, MAX, PREMIUM]
  REQUIRED_FIELD  = I18n.t('messages.errors.required_field')

  has_and_belongs_to_many :benefits
  has_many :orders

  validates :name,
            :price,
            presence: { message: REQUIRED_FIELD }

  def self.total_price(students_transported, plan_price)
    students_transported.to_f * plan_price
  end

  def self.description(plan)
    I18n.t('models.payment.plan_signature', plan: plan.titleize)
  end

  def self.availability_description(student_amount)
    I18n.t('models.payment.availability_description',
           student_amount: student_amount)
  end
end
