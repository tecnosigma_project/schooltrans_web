class Benefit < ActiveRecord::Base
  REQUIRED_FIELD  = I18n.t('messages.errors.required_field')

  has_and_belongs_to_many :plans

  validates :description,
            presence: { message: REQUIRED_FIELD }
end
