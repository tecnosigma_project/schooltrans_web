class Status
  STATUSES = { active: 'ativo',
               pendent: 'pendente',
               deactivated: 'desativado' }.freeze
end
