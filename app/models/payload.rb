class Payload
  ACCOUNT_TYPE = '2'.freeze # Enterprise
  API_ERROR_MESSAGE = 'error'.freeze
  USER_NOT_FOUND = 'Cliente não encontrado'.freeze
end
