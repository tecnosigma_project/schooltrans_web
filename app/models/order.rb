class Order < ActiveRecord::Base
  REQUIRED_FIELD  = I18n.t('messages.errors.required_field')

  belongs_to :plan

  validates :price,
            :status,
            :students_transported,
            :seller,
            :school,
            presence: { message: REQUIRED_FIELD }
end
