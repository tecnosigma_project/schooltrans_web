$(document).ready(function() {
  $(function() {
    $('#school_password').attr('disabled', 'disabled');
    $('#confirm_school_password').attr('disabled', 'disabled');
    $('#continue_in_resume_page').attr('disabled', 'disabled');

    maskedInputs();

    getClientIp();
  });

  $('#school_user').focus(function(){
    $('#school_password').attr('disabled', 'disabled');
    $('#confirm_school_password').attr('disabled', 'disabled');
  });

  $('#school_postal_code').blur(function(){
    $.ajax({
      type: 'GET',
      url: '/school_coordinates',
      data: { address: $('#school_address').val(),
              number: $('#school_number').val(),
              district: $('#school_district').val(),
              city: $('#school_city').val(),
              state: $('#school_state').val(),
              postal_code: $('#school_postal_code').val() },
      success: function(data,status,xhr){
        if (data.latitude != null && data.longitude != null){
          $('#latitude').val(data.latitude);
          $('#longitude').val(data.longitude);
        }
      },
      error: function(xhr,status,error){
        console.log(xhr);
      }
    });
  });

  $('#school_user').blur(function(){
    $.ajax({
      type: 'GET',
      url: '/school_user_availability',
      data: { user: $(this).val() },
      success: function(data,status,xhr){
        if (data.available){
          $('#available_school_user').removeClass('hidden-messages');
          $('#unavailable_school_user').addClass('hidden-messages');

          $('#school_password').removeAttr('disabled');
          $('#confirm_school_password').removeAttr('disabled');
        }else{
          $('#available_school_user').addClass('hidden-messages');
          $('#unavailable_school_user').removeClass('hidden-messages');

          $('#school_password').focus();
        }
      },
      error: function(xhr,status,error){
        console.log(xhr);
      }
    });
  });

  $('#school_password').bind('input propertychange', function() {
    var user     = $('#school_user').val();
    var password = $(this).val();
    var strength = PasswordStrength.test(user, password);

    if (strength.isGood() || strength.isStrong()) {
      $('#fragile_password').addClass('hidden-messages');
      $('#strong_password').removeClass('hidden-messages');

      $('#confirm_school_password').removeAttr('disabled');
    } else {
      $('#fragile_password').removeClass('hidden-messages');
      $('#strong_password').addClass('hidden-messages');

      $('#confirm_school_password').attr('disabled', 'disabled');
    }
  });

  $('#school_password').focus(function(){
    $(this).val('');
    $('#confirm_school_password').val('');
  });

  $('#confirm_school_password').bind('input propertychange', function() {
    if ($(this).val() == $('#school_password').val()) {
      $('#continue_in_resume_page').removeAttr('disabled');
    } else {
      $('#continue_in_resume_page').attr('disabled', 'disabled');
    }
  });

  $('#seller_id').bind('input propertychange', function(){
    if ($(this).val().length == 0) {
      $('#seller_loader_image').addClass("hidden-loader");
      $('#seller_not_found').addClass("hidden-messages");

      $('#seller_name').val('');
    }

    if ($(this).val().length > 0 && $(this).val().length < 14) {
      $('#seller_loader_image').addClass("hidden-loader");
      $('#seller_not_found').addClass("hidden-messages");

      $('#seller_name').val('');
    }

    if ($(this).val().length == 14) {
      $('#seller_loader_image').removeClass("hidden-loader");

      $.ajax({
        type: 'GET',
        url: '/seller_name',
        data: { seller_id: $(this).val() },
        success: function(data,status,xhr){

          $('#seller_loader_image').addClass("hidden-loader");

          if (data.seller_name == '') {
            $('#seller_not_found').removeClass('hidden-messages');
          } else {
            $('#seller_not_found').addClass("hidden-messages");
            $('#seller_name').val(data.seller_name);
          }
        },
        error: function(xhr,status,error){
          console.log(xhr);
        }
      });
    }
  });

  $('#school_postal_code').bind('input propertychange', function(){
    if ($(this).val().length == 9) {

      $('#postal_code_loader_image').removeClass("hidden-loader");

      $.ajax({
        type: 'GET',
        url: '/checkout/school_address',
        data: { postal_code: $(this).val() },
        success: function(data,status,xhr){

          $('#postal_code_loader_image').addClass("hidden-loader");

          $('#school_address').val(data.address);
          $('#school_district').val(data.neighborhood);
          $('#school_city').val(data.city);
          $('#school_state').val(data.state);
        },
        error: function(xhr,status,error){
          console.log(xhr);
        }
      });
    }
  });

  $('#school_email').blur(function(){
    if (ValidateEmail($(this).val())) {
      $('#invalid_email_message').addClass('hidden-messages');
    } else {
      $('#invalid_email_message').removeClass('hidden-messages');
      $('#school_email').val('');
      $('#school_email').focus();
    }
  });

  $('#students_transported').blur(function(){
    if (parseInt($(this).val()) == 0 || $(this).val() == "") {
      $(this).val('');
      $(this).focus();
    }
  });
});

function ValidateEmail(email) {
  if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
    return true;
  }
  return false;
};

function getClientIp() {
  window.RTCPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;

  var pc = new RTCPeerConnection({iceServers:[]}), noop = function(){};
  pc.createDataChannel("");
  pc.createOffer(pc.setLocalDescription.bind(pc), noop);
  pc.onicecandidate = function(ice) {
    if (!ice || !ice.candidate || !ice.candidate.candidate) return;

    var myIp = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/.exec(ice.candidate.candidate)[1];

    $('#client_ip').val(myIp);
    pc.onicecandidate = noop;
  }
}

function maskedInputs() {
  $('#school_cnpj')
    .mask('00.000.000/0000-00',
          { placeholder: '__.___.___/____-__' });

  $('#responsible_cpf')
    .mask('000.000.000-00',
          { placeholder: '___.___.___-__' });

  $('#seller_id')
    .mask('000.000.000-00',
          { placeholder: '___.___.___-__' });

  $('#school_postal_code')
    .mask('00000-000',
          { placeholder: '_____-___' });

  $('#school_telephone')
    .mask('(00) 0000 0000',
          { placeholder: '(__) ____-____' });

  $('#students_transported')
    .mask('9999',
          { placeholder: '____' });
}
