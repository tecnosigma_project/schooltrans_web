# Parent mailer
class ApplicationMailer < ActionMailer::Base
  default from: ENV['default_email']
end
