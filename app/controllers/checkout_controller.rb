# Responsible controller by app checkout
class CheckoutController < ApplicationController
  before_filter :step_to_hiring, only: %i[identification resume payment]

  def identification
    return redirect_to root_path if params['plan'].nil?

    redirect_to root_path unless exist_plan?

    session[:plan_name] = params['plan'].titleize
  end

  def resume
    @benefit_list = Plan.find_by_name(session[:plan_name].titleize).benefits
    @school_data = session[:school_data]
    @plan_name = session[:plan_name]
  end

  def payment
    unless exist_user?
      result = create_new_gateway_school!

      add_token_account!(result)
    end

    response = Gateways::Yapay::Payment.send_payload(session[:school_data])

    if response.code == Rack::Utils::HTTP_STATUS_CODES.key('Created')
      # Gravar dados da escola
      @email = session[:school_data]['school']['contact']['email']
      @plan_name = session[:school_data]['plan']
    end
  end

  def school_data
    return redirect_to checkout_identification_path if session[:plan_name].nil?

    session[:school_data] = Builders::Payloads::School
                              .data(session[:plan_name], params)

    redirect_to checkout_resume_path
  end

  def school_coordinates
    address_hash = { address: params[:address],
                     number: params[:number],
                     district: params[:district],
                     city: params[:city],
                     state: params[:state],
                     postal_code: params[:postal_code] }

    render json:   Coordinates.parsed_coordinates(address_hash),
           status: :ok
  end

  def seller_name
    response = RestClient.post(uri_to_get_seller_name,
                               'seller_id' => params['seller_id'])

    @result = if response.code == Rack::Utils::HTTP_STATUS_CODES.key('OK')
                seller_payload(JSON.parse(response.body)['seller_name'])
              else
                seller_payload
              end

    render json: @result, status: :ok
  rescue StandardError
    render json: seller_payload, status: :ok
  end

  def school_user_availability
    response = RestClient
                 .post(uri_to_check_user_availability, 'user' => params['user'])

    @result = if response.code ==  Rack::Utils::HTTP_STATUS_CODES.key('OK')
                availability_payload(JSON.parse(response.body)['school'])
              else
                { 'available' => false }
              end

    render json: @result, status: :ok
  rescue StandardError
    render json: { 'available' => false }, status: :ok
  end

  def school_address
    render json: address.as_json,
           status: :ok
  rescue StandardError
    render json: Hash.new,
           status: :not_found
  end

  private

  def exist_user?
    cpf = session[:school_data]['school']['responsible_cpf']
    email = session[:school_data]['school']['contact']['email']

    user_payload = Builders::Payloads::School
                     .get_user_data(cpf: cpf, email: email)

    response = RestClient
                 .post(APP_CONFIG['yapay_api']['client_search_uri'],
                       user_payload)

     xml_doc = Nokogiri::XML(response.body)

     xml_doc.xpath('//message').first.text != Payload::USER_NOT_FOUND &&
     xml_doc.xpath('//message').last.text != Payload::API_ERROR_MESSAGE
  end

  def step_to_hiring
    @step = action_name
  end

  def address
    address = Correios::CEP::AddressFinder.get(params['postal_code'])

    return address if address == Hash.new

    address[:state] = States.state_name(address[:state])

    address
  end

  def exist_plan?
    Plan.find_by_name(params['plan'].titleize).present?
  end

  def uri_to_get_seller_name
    "#{APP_CONFIG['api_uri']['sellercentral']}seller/get_name"
  end

  def uri_to_check_user_availability
    "#{APP_CONFIG['api_uri']['monitoringcentral']}school/user_availability"
  end

  def seller_payload(seller_name = nil)
    if seller_name.present?
      { 'seller_name' => seller_name }
    else
      { 'seller_name' => '' }
    end
  end

  def availability_payload(school_data = nil)
    school_data.present? ? { 'available' => false } : { 'available' => true }
  end

  def create_new_gateway_school!
    new_school = Builders::Payloads::School.create_user(session[:school_data])
    RestClient.post(APP_CONFIG['yapay_api']['client_creation_uri'], new_school)
  end

  # def get_token_account(result)
  #   Nokogiri::XML(result.body).xpath('//token_account').first.text
  # end
  #
  # def add_token_account!(result)
  #   binding.pry
  #   if Nokogiri::XML(result.body).xpath('//message').first.text == 'success'
  #     session[:school_data]
  #       .merge!({ 'token_account' => get_token_account(result) })
  #   end
  # end
end
