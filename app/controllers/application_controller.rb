# Parent class
class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  skip_before_action :verify_authenticity_token

  def validate_session(profile)
    return if session[:user].present?

    redirect_to drivers_dashboard_login_path if profile == 'Drivers'
    redirect_to schools_dashboard_login_path if profile == 'Schools'
  end

  def namespace(controller_data)
    controller_data.class.to_s.split('::').first
  end

  def ensure_json_request
    return if request.format == :json
    render nothing: true, status: :not_acceptable
  end
end
