# Parent helper
module ApplicationHelper
  def formatted_date(date)
    date.strftime('%d/%m/%Y')
  end

  def formatted_time(time)
    time.strftime('%H:%M')
  end

  def boolean_options
    [['Não', 0], ['Sim', 1]]
  end

  def bool_to_integer(boolean_value)
    boolean_value ? 1 : 0
  end

  def bool_to_string(boolean_value)
    boolean_value ? 'Sim' : 'Não'
  end

  def state_list
    States.all_names
  end
end
