# Responsible by home helper
module HomeHelper
  def plans_benefit
    table_content = ''


    Benefit.all.each do |benefit|
      description = benefit.description
      table_content.concat("<tr><td>#{benefit.description}</td>" \
                           "#{load_image(Plan::BASIC, description)}" \
                           "#{load_image(Plan::MAX, description)}" \
                           "#{load_image(Plan::PREMIUM, description)}" \
                           '</tr>')
    end

    table_content
  end

  def load_image(type_plan, benefit)
    if Plan
        .find_by_name(type_plan).benefits.pluck(:description).include?(benefit)
      return field_with_image('yes', 'Sim')
    else
      return field_with_image('no', 'Não')
    end
  end

  def plans_name
    table_header = ''

    Plan.pluck(:name).each do |plan_name|
      table_header.concat("<th>#{plan_name}</th>")
    end

    table_header
  end

  def plans_price
    price_labels = ''

    Plan.pluck(:price).each do |price|
      price_labels
        .concat("<td>#{convert_to_currency(price) + Plan::UNIT}</td>")
    end

    price_labels
  end

  def field_with_image(image_name, description)
    "<td><img src='images/#{image_name}.png' alt='#{description}'></td>"
  end

  def convert_to_currency(number)
    number_to_currency(number, unit: 'R$', separator: ',', delimiter: '.')
  end
end
