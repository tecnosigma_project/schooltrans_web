# Responsible by helper of checkout page
module CheckoutHelper
  def school_types
    School::SCHOOL_KINDS
  end

  def information_rule(step)
    case step
    when 'identification'
      identification_marker
    when 'resume'
      resume_marker
    when 'payment'
      payment_marker
    end
  end

  def identification_marker
    "<li class='actived-step'>
    #{t('partials.information_rule.identification')}
    </li>
    <li><span class='glyphicon glyphicon-play'></li>
    <li class='deactivated-step'>#{t('partials.information_rule.resume')}</li>
    <li><span class='glyphicon glyphicon-play'></li>
    <li class='deactivated-step'>#{t('partials.information_rule.payment')}
    </li>".html_safe
  end

  def resume_marker
    "<li class='deactivated-step'>
    #{t('partials.information_rule.identification')}
    </li>
    <li><span class='glyphicon glyphicon-play'></li>
    <li class='actived-step'>#{t('partials.information_rule.resume')}</li>
    <li><span class='glyphicon glyphicon-play'></li>
    <li class='deactivated-step'>
    #{t('partials.information_rule.payment')}
    </li>".html_safe
  end

  def payment_marker
    "<li class='deactivated-step'>
    #{t('partials.information_rule.identification')}
    </li>
    <li><span class='glyphicon glyphicon-play'></li>
    <li class='deactivated-step'>#{t('partials.information_rule.resume')}</li>
    <li><span class='glyphicon glyphicon-play'></li>
    <li class='actived-step'>
    #{t('partials.information_rule.payment')}
    </li>".html_safe
  end
end
