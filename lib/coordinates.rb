require 'open-uri'
require 'i18n'

# Address coordinates
module Coordinates
  class << self
    def parsed_coordinates(address)
      coordinates = by_address(address)

      if coordinates == { latitude: empty_string, longitude: empty_string }
        by_postal_code(address[:postal_code])
      else
        coordinates
      end
    rescue StandardError
      { latitude: empty_string, longitude: empty_string }
    end

    private

    def by_address(address)
      get_coordinates(address: address[:address],
                      number: address[:number],
                      district: address[:district],
                      city: address[:city],
                      state: address[:state])
    end

    def by_postal_code(postal_code)
      address = Correios::CEP::AddressFinder.get(postal_code.delete('-'))

      get_coordinates(address: address[:address],
                      number: nil,
                      district: address[:neighborhood],
                      city: address[:city],
                      state: address[:state])
    end

    def get_coordinates(address:, number:, district:, city:, state:)
      google_api_url = Builders::Google::GeocodeBuilder
                       .url(address: address,
                            number: number,
                            district: district,
                            city: city,
                            state: state)

      parsed_url = JSON.parse(open(I18n.transliterate(google_api_url)))

      { latitude: parsed_url['results'][0]['geometry']['location']['lat'],
        longitude: parsed_url['results'][0]['geometry']['location']['lng'] }
    end
  end

  def empty_string
    ''
  end
end
