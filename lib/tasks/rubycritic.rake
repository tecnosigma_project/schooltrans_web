desc 'Rubycritic'
task :rubycritic do
  system 'bundle exec rubycritic --format html --minimum-score 87 lib'
end
