# Gateways
module Gateways
  # Gateway Company
  module Yapay
    # responsible by payments
    module Payment
      class << self
        def send_payload(client_data)
          RestClient.post(APP_CONFIG['yapay_api']['payment_uri'],
                          generate_payload(client_data))
        end

        private

          def generate_payload(client_data)
            Builders::Payloads::Payment.data(client_data)
          end
      end
    end
  end
end
