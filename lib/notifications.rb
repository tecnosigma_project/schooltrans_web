# Responsible by notifications
class Notifications
  NOTIFICATION_SENT_SUCESSFULLY = I18n.t('messages
                                          .success
                                          .notification_sent_successfully')

  def self.send_notifications(student_data, checking_params)
    message = if checking_params['absent'] == 'true'
                absent_notification(checking_params)
              else
                checking_notification(checking_params)
              end

    cellphone = formatted_cellphone(student_data.responsible_cellphone)

    return notification_sent_json if send_sms(telephone: cellphone,
                                              message: message)

    return notification_sent_json if send_email(student_data: student_data,
                                                message: message)
  end

  def self.formatted_cellphone(cellphone)
    cellphone
      .responsible_cellphone
      .delete('(')
      .delete(')')
      .delete(' ')
      .delete('-')
  end

  # def self.send_sms(telephone:, message:)
  #   # SendSmsJob.perform_now(telephone:, message:)
  #   false
  # end

  def self.send_email(student_data:, message:)
    Notifications::Checking
      .send_checking(student_data, message)
      .deliver_now!
  end

  def self.notification_sent_json
    { 'notification_sent' => true }
  end

  def self.absent_notification(notification_params)
    student_name = Student.find(notification_params['student_id']).name

    I18n.t('notifications.checking_messages.absent',
           student_name: student_name,
           absent_justification: notification_params['justification'])
  end

  def self.checking_notification(notification_params)
    event_time = (DateTime.now + ENV['time_zone'].to_i.hours)
                 .strftime('%H:%M:%S')
    student_name = Student.find(notification_params['student_id']).name

    check_out_home(notification_params, student_name, event_time)
    check_in_school(notification_params, student_name, event_time)
    check_out_school(notification_params, student_name, event_time)
    check_in_home(notification_params, student_name, event_time)
  end

  def self.check_out_home(notification_params, student_name, event_time)
    if notification_params
       .key?('check_out_home')
      I18n.t('notifications.checking_messages.check_out_home',
             student_name: student_name,
             event_time: event_time)
    end
  end

  def self.check_in_school(notification_params, student_name, event_time)
    if notification_params
       .key?('check_in_school')
      I18n.t('notifications.checking_messages.check_in_school',
             student_name: student_name,
             event_time: event_time)
    end
  end

  def self.check_out_school(notification_params, student_name, event_time)
    if notification_params
       .key?('check_out_school')
      I18n.t('notifications.checking_messages.check_out_school',
             student_name: student_name,
             event_time: event_time)
    end
  end

  def self.check_in_home(notification_params, student_name, event_time)
    if notification_params
       .key?('check_in_home')
      I18n.t('notifications.checking_messages.check_in_home',
             student_name: student_name,
             event_time: event_time)
    end
  end

  private_class_method :formatted_cellphone,
                       :send_email,
                       :notification_sent_json,
                       :absent_notification,
                       :checking_notification,
                       :check_out_home,
                       :check_in_school,
                       :check_out_school,
                       :check_in_home
end
