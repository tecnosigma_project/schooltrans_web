module Regex
  class << self
    def cpf
      /\A\d{3}.\d{3}.\d{3}-\d{2}\z/
    end

    def cnpj
      /\A\d{2}.\d{3}.\d{3}\/\d{4}-\d{2}\z/
    end

    def cnh
      /\A\d{2}.\d{3}.\d{3}.\d{3}\z/
    end

    def renavam
      /\A\d{2}.\d{3}.\d{3}.\d{3}\z/
    end

    def license_plate
      /\A[A-Z]{3}-\d{4}\z/
    end

    def year
      /\A\d{4}\z/
    end

    def email
      /\A[a-z0-9._-]+@[a-z0-9]+.[a-z0-9.]+.[a-z0-9._-]*\z/
    end

    def postal_code
      /\A\d{5}-\d{3}\z/
    end

    def telephone
      /\A\([1-9]{2}\) 9?[0-9]{4}\ [0-9]{4}\z/
    end

    def cellphone
      /\A\([1-9]{2}\) 9 [0-9]{4}\ [0-9]{4}\z/
    end

    def password
      /\A[a-zA-Z0-9]{10,20}\z/
    end

    def user
      /\A[a-zA-Z0-9]{6,12}\z/
    end
  end
end
