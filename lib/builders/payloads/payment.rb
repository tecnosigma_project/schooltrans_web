# Builders
module Builders
  # Payload builders
  module Payloads
    # Payment data
    module Payment
      class << self
        def data(client_data)
          {
            token_account: APP_CONFIG['yapay_api']['token_account'],
            customer:{
              contacts: [contact(client_data)],
              addresses: [address(client_data)],
              name: client_data['school']['name'],
              cpf: formatted_cpf(client_data['school']['responsible_cpf']),
              cnpj: client_data['school']['cnpj'],
              email: client_data['school']['contact']['email']
            },
            transaction_product: [transaction_product(client_data)],
            transaction: transaction(client_data),
            payment: payment(client_data)
          }
        end

        def contact(client_data)
          {
            type_contact: ::Payment::COMMERCIAL_CONTACT,
            number_contact: client_data['school']['contact']['telephone']
                              .gsub('(','')
                              .gsub(')','')
                              .gsub(' ','')
          }
        end

        def address(client_data)
          {
            type_address: ::Payment::COMMERCIAL_ADDRESS,
            postal_code: client_data['school']['address']['postal_code'],
            street: client_data['school']['address']['address'],
            number: client_data['school']['address']['number'],
            completion: client_data['school']['address']['complement'],
            neighborhood: client_data['school']['address']['district'],
            city: client_data['school']['address']['city'],
            state: formatted_state(client_data['school']['address']['state'])
          }
        end

        def transaction_product(client_data)
          student_amount = client_data['school']['students_transported']

          {
            description: Plan.description(client_data['plan']),
            quantity: ::Payment::PLAN_AMOUNT,
            price_unit: formatted_price(client_data),
            code: client_data['plan'],
            extra: Plan.availability_description(student_amount)
          }
        end

        def transaction(client_data)
          {
            available_payment_methods: ::Payment::AVAILABLE_TYPE,
            customer_ip: client_data['school']['ip'],
            url_notification: APP_CONFIG['payment_notifications']
          }
        end

        def payment(client_data)
          { payment_method_id: ::Payment::AVAILABLE_TYPE }
        end

        def formatted_price(client_data)
          student_amount = client_data['school']['students_transported']
          plan_price = Plan.find_by_name(client_data['plan'].titleize).price

          sprintf("%.2f",
                  (Plan.total_price(student_amount, plan_price)))
        end

        def formatted_cpf(client_data)
          client_data.gsub('.','').gsub('-','')
        end

        def formatted_state(state)
          state_list = { ac: 'Acre',
                         al: 'Alagoas',
                         ap: 'Amapá',
                         am: 'Amazonas',
                         ba: 'Bahia',
                         ce: 'Ceará',
                         df: 'Distrito Federal',
                         es: 'Espírito Santo',
                         go: 'Goiás',
                         ma: 'Maranhão',
                         mt: 'Mato Grosso',
                         ms: 'Mato Grosso do Sul',
                         mg: 'Minas Gerais',
                         pr: 'Paraná',
                         pb: 'Paraíba',
                         pa: 'Pará',
                         pe: 'Pernambuco',
                         pi: 'Piauí',
                         rs: 'Rio Grance do Sul',
                         rn: 'Rio Grande do Norte',
                         rj: 'Rio de Janeiro',
                         ro: 'Rondônia',
                         rr: 'Roraima',
                         sc: 'Santa Catarina',
                         se: 'Sergipe',
                         sp: 'São Paulo',
                         to: 'Tocantins' }

          state_list.key(state).to_s.upcase
        end
      end
    end
  end
end
