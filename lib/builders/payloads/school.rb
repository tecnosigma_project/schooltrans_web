# Builders
module Builders
  # Payload builders
  module Payloads
    # School data
    module School
      class << self
        def get_user_data(cpf:, email:)
          {
            cpf: cpf,
            email: email
          }
        end

        def create_user(params)
          {
            account_type: ::Payload::ACCOUNT_TYPE,
            trade_name: params['school']['name'],
            company_name: params['school']['name'],
            cnpj: params['school']['cnpj'],
            email: params['school']['contact']['email'],
            name: params['school']['responsible_name'],
            cpf: formatted_cpf(params['school']['responsible_cpf']),
            contacts: [contact(params['school']['contact']['telephone']) ],
            type_address: ::Payment::COMMERCIAL_ADDRESS,
            street: params['school']['address']['address'],
            number: params['school']['address']['number'],
            neighborhood: params['school']['address']['district'],
            postal_code: params['school']['address']['postal_code'],
            city: params['school']['address']['city'],
            state: formatted_state(params['school']['address']['state'])
          }
        end

        def data(plan_name, params)
          { 'plan' => plan_name,
            'seller' => { 'id' => params['seller_id'] },
            'school' => { 'name' => params['name'],
                          'responsible_cpf' => params['responsible_cpf'],
                          'responsible_name' => params['responsible_name'],
                          'cnpj' => params['cnpj'],
                          'kind' => params['kind'],
                          'students_transported' => params['students_transported'],
                          'address' => school_address(params),
                          'contact' => school_contact(params),
                          'access' => school_access(params),
                          'status' => Status::STATUSES[:pendent],
                          'ip' => params['client_ip'] } }
        end

        private

          def school_contact(params)
            { 'email' => params['email'],
              'telephone' => params['telephone'] }
          end

          def school_access(params)
            { 'user' => params['user'],
              'password' => params['password'] }
          end

          def school_address(params)
            { 'postal_code' => params['postal_code'],
              'address' => params['address'],
              'number' => params['number'],
              'complement' => params['complement'],
              'district' => params['district'],
              'city' => params['city'],
              'state' => params['state'],
              'latitude' => params['latitude'],
              'longitude' => params['longitude'] }
          end

          def contact(client_data)
            {
              type_contact: ::Payment::COMMERCIAL_CONTACT,
              number_contact: client_data
                                .gsub('(','')
                                .gsub(')','')
                                .gsub(' ','')
            }
          end

          def formatted_state(state)
            state_list = { ac: 'Acre',
                           al: 'Alagoas',
                           ap: 'Amapá',
                           am: 'Amazonas',
                           ba: 'Bahia',
                           ce: 'Ceará',
                           df: 'Distrito Federal',
                           es: 'Espírito Santo',
                           go: 'Goiás',
                           ma: 'Maranhão',
                           mt: 'Mato Grosso',
                           ms: 'Mato Grosso do Sul',
                           mg: 'Minas Gerais',
                           pr: 'Paraná',
                           pb: 'Paraíba',
                           pa: 'Pará',
                           pe: 'Pernambuco',
                           pi: 'Piauí',
                           rs: 'Rio Grance do Sul',
                           rn: 'Rio Grande do Norte',
                           rj: 'Rio de Janeiro',
                           ro: 'Rondônia',
                           rr: 'Roraima',
                           sc: 'Santa Catarina',
                           se: 'Sergipe',
                           sp: 'São Paulo',
                           to: 'Tocantins' }

            state_list.key(state).to_s.upcase
          end

          def formatted_cpf(client_data)
            client_data.gsub('.','').gsub('-','')
          end
      end
    end
  end
end
