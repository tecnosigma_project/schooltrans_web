# builder
module Builders
  # Google
  module Google
    # Google Geocode
    module GeocodeBuilder
      class << self
        def url(address:, number:, district:, city:, state:)
          "#{APP_CONFIG.fetch('google_api').fetch('geocode_url')}?address=" \
          "#{formatted_address(address, number, district, city, state)}" \
          "#{APP_CONFIG.fetch('google_api').fetch('sensor')}"
        rescue StandardError => e
          Rails.logger.error "Message: #{e.message} - Backtrace: #{e.backtrace}"
          empty_builder
        end

        private

        def empty_builder
          ''
        end

        def formatted_address(address, number, district, city, state)
          "#{address.tr(' ', '+')}+" \
          "#{number}+" \
          "#{district.tr(' ', '+')}+" \
          "#{city.tr(' ', '+')}+" \
          "#{state.tr(' ', '+')}"
        end
      end
    end
  end
end
