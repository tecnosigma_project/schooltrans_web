Rails.application.routes.draw do
  root 'home#index'

  get 'checkout/identification', to: 'checkout#identification'
  get 'checkout/resume', to: 'checkout#resume'
  get 'checkout/payment', to: 'checkout#payment'
  get 'checkout/school_address', to: 'checkout#school_address'

  get 'school_coordinates', to: 'checkout#school_coordinates'
  get 'school_user_availability', to: 'checkout#school_user_availability'
  get 'seller_name', to: 'checkout#seller_name'

  post 'checkout/school_data', to: 'checkout#school_data'
end
